# rocket_sqlx_migrator

Use this crate with the `rocket` web framework and `rocket_db_pools` to automatically run migrations on startup using a faring and some generic type magic.

All you have to do is define a db type using `rocket_db_pools`:

```rust
#[derive(Database)]
#[database("times")]
struct MainDB(Pool<Sqlite>);

```
and then register it to the migrator using:

```rust
#[launch]
fn rocket() -> _ {
    rocket::build()
        .attach(MainDB::init())
        .attach(SqlxMigrator::<MainDB>::migrate())
}

```

Note: You can't currently change the migrations folder away from the default, found at `./migrations`.

FWI: I'm looking for some help to create some tests!

Note2myself: use cargo readme to generate readme

