//! Use this crate with the `rocket` web framework and `rocket_db_pools` to automatically run migrations on startup using a faring and some generic type magic.
//!
//! All you have to do is define a db type using `rocket_db_pools`:
//!
//! ```
//! #[derive(Database)]
//! #[database("times")]
//! struct MainDB(Pool<Sqlite>);
//!
//! ```
//! and then register it to the migrator using:
//! 
//! ```
//! #[launch]
//! fn rocket() -> _ {
//!     rocket::build()
//!         .attach(MainDB::init())
//!         .attach(SqlxMigrator::<MainDB>::migrate())
//! }
//! 
//! ```
//! 
//! Note: You can't currently change the migrations folder away from the default, found at `./migrations`.
//! 
//! FWI: I'm looking for some help to create some tests!
//! 
//! Note2myself: use cargo readme to generate readme
//! 

use std::{marker::PhantomData, ops::Deref, path::Path};

use rocket::{
    error,
    fairing::{self, Fairing, Info, Kind},
    Build, Rocket,
};
use sqlx::{
    migrate::{Migrate, Migrator},
    Acquire,
};

/// See root level documentation for how to use this struct.
/// 
/// Fun fact: The `DB` Generic is implemented with the `PhantomData` Struct.
pub struct SqlxMigrator<DB>(PhantomData<DB>);

use rocket_db_pools::Database;

impl<DB> SqlxMigrator<DB> {
    pub fn migrate() -> Self {
        SqlxMigrator(PhantomData::<DB>)
    }
}

#[rocket::async_trait]
impl<DB> Fairing for SqlxMigrator<DB>
where
    DB: Send + Sync + 'static + Database,
    for<'a> &'a <DB as Database>::Pool: Acquire<'a>,
    for<'a> <<&'a <DB as Database>::Pool as Acquire<'a>>::Connection as Deref>::Target: Migrate,
{
    fn info(&self) -> Info {
        Info {
            name: "Sqlx Migrations",
            kind: Kind::Ignite,
        }
    }

    async fn on_ignite(&self, rocket: Rocket<Build>) -> fairing::Result {
        match DB::fetch(&rocket) {
            Some(db) => match Migrator::new(Path::new("./migrations"))
                .await
                .unwrap() // This needs changing to a more graceful system
                .run(&**db)
                .await
            {
                Ok(_) => Ok(rocket),
                Err(e) => {
                    error!("Failed to run database migrations: {}", e);
                    Err(rocket)
                }
            },
            None => Err(rocket),
        }
    }
}
